#!/usr/bin/python

from numpy import *
import matplotlib.pyplot as plt

lag, acf, _ = loadtxt("autocor.out", unpack=True)

plt.plot(lag, acf, 'k-')
plt.xlabel("Lag (days)")
plt.ylabel("Auto-correlation function")
plt.savefig("autocor.png")
plt.show()
