#!/usr/bin/python


#Turns a sigproc file into the correct flux matrix required by Valign.py
#This script does not discriminate on telescope, frequency or backend so be careful!
#You can use filter_sigproc.py to create a bad mjds file to exclude irrelevant observations

import sys
import re

data = {}
with open(sys.argv[1], 'r') as infile:
    for line in infile.readlines():
        line=line.strip()
        if line.startswith('#'):
            cols=re.search('([0-9]{3,})', line)
            mjd=cols.group(1)
            data[mjd]=[]
        else:
            cols=re.search('[0-9][ ]{1,}([0-9E+-.]{1,})', line)
            data[mjd].append(cols.group(1))

number_of_bins = float(sys.argv[2])
x=0 
while x<number_of_bins:
    line=""
    for key in sorted(data.keys()):
        line=line+data[key][x]+"\t"
    x=x+1
    print line

line=""
for key in sorted(data.keys()):
    line=line+"3000.000\t"
print line
 
line=""
for key in sorted(data.keys()):
    line=line+key+"\t"
print line
