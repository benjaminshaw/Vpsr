#!/usr/bin/python

from numpy import *
import matplotlib.pyplot as plt
import sys

mjd = sys.argv[1]
nudot = sys.argv[2]
err = sys.argv[3]
lyne = sys.argv[4]

x = loadtxt(mjd, unpack=True, skiprows=1)
y = loadtxt(nudot, unpack=True, skiprows=1)
yerr = loadtxt(err, unpack=True, skiprows=1)

mjdlyne, _, _, _, _, lyney, lynez, lyneerr = loadtxt(lyne, unpack=True)


ratio = 1e-15
print "Lyne Data"
print "Max value of nudot:", max(lyney)*ratio
print "Min value of nudot:", min(lyney)*ratio
print "Mean value of nudot:", mean(lyney)*ratio

print "Mean nudot of our data", mean(y)
plt.errorbar(x, y/ratio, yerr=yerr/ratio, marker='.', color='k', ecolor='k', label="This work")
plt.errorbar(mjdlyne, lyney, yerr=lyneerr, marker='.', color='b', ecolor='b', linestyle='dashed', label="Lyne (2010)")
plt.xlabel("Modified Julian Day")
plt.ylabel(r'$\dot{\nu}$ (1e-15 Hz/s)')
plt.legend(loc=0)
#plt.xlim([50000, 54000])
#plt.ylim([-610, -595]) #uncomment for 0740

plt.savefig("lyne_comp.png")
plt.show()
