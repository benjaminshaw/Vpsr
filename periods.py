#!/usr/bin/python

from numpy import *
from scipy import signal
import matplotlib.pyplot as plt
import sys
from astropy.stats import LombScargle
set_printoptions(threshold=inf)


mjd = sys.argv[1]
nudot = sys.argv[2]
nudot_err = sys.argv[3]


#t, y, yerr = loadtxt("residuals_post_fit_F0_F1_RA_DEC.dat", unpack=True)
t = loadtxt(mjd, unpack=True)
y = loadtxt(nudot, unpack=True)
yerr = loadtxt(nudot_err, unpack=True)

first_mjd = t[0]
time_year = []
for i in range(0, len(t)):
    time_day = t[i] - first_mjd
    time_year.append(time_day/365.25)

time_year = asarray(time_year)
#for i in range(0, len(time_year)):
#    print time_year[i], y[i]


frequency, power = LombScargle(time_year, y, yerr).autopower(minimum_frequency=0.01, maximum_frequency=4.0, samples_per_peak=10)

maxpowerbin = argmax(power)

plt.xlabel('Frequency (1/yr)')
plt.ylabel('Normalised spectral power')
plt.plot(frequency, power/power[maxpowerbin], 'k-')
plt.savefig("LS.png")
plt.show()

plt.xlabel('Frequency (1/yr)')
plt.ylabel('Normalised spectral power')
plt.plot(frequency, power, 'k-')
plt.savefig("LS_unnorm.png")
plt.show()


fp_array = []
print "Sampled", len(frequency), "frequencies"
for i in range(0, len(frequency)):
    fp_array.append([frequency[i], power[i]])
    if power[i] == max(power):
        print "Maximum power at", frequency[i], "per year"

bin_width = 4.0 / len(frequency)
half_bin_width = bin_width / 2.0
print "Uncertainty:", half_bin_width

with open("spectral_power.txt", 'a') as spec:
    for i in range(0, len(frequency)):
        spec.write(str(frequency[i]) + " " + str((power[i])/power[maxpowerbin]) + "\n")

norm_freq, norm_power = loadtxt("spectral_power.txt", unpack=True)


with open("power_maxima.txt", 'a') as pout:
    for i in range(1, len(norm_freq) - 1):
        if norm_power[i] > norm_power[i-1] and norm_power[i] > norm_power[i+1]:
            pout.write(str(norm_freq[i]) + " " + str(norm_power[i]) + "\n")
