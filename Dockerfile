##############################################################################################
## Purpose   : Dockerize Vpsr
## Author    : Benjamin Shaw (benjamin.shaw@manchester.ac.uk)
###############################################################################################

# Select base OS
FROM ubuntu:bionic

LABEL author="Shaw B"

# WORKDIR sets the location in the container 
# where subsequent commands are run
WORKDIR /home

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London

RUN apt-get -y update && apt-get -y install \
    tcsh \ 
    unzip \ 
    wget \ 
    make \ 
    autoconf \
    automake \ 
    gcc \
    gfortran \
    libtool \
    g++ \
    git \ 
    eog \
    evince \ 
    xterm  \
    swig \
    python2.7-dev \ 
    pgplot5 \
    python-tk \
    python-pip \
    vim

RUN pip install  --upgrade pip  && \
    pip install  pip -U  && \
    pip install  setuptools -U  && \
    pip install  numpy -U && \
    pip install  scipy -U  && \
    pip install  matplotlib -U && \
    pip install  pyephem -U && \
    pip install GPy==1.9.5 -U

WORKDIR /home
RUN git clone https://gitlab.com/benjaminshaw/Vpsr.git
ENV MPLBACKEND='Agg'

WORKDIR /home/Vpsr
ENTRYPOINT /bin/bash
