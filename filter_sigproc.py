#!/usr/bin/python

#This file uses the header info from each observation in a sigproc file to compile a list of mjds which are irrelevant based on telescope and frequency. Care needs to be taken as bandwidth is not counted.

import sys
from numpy import *

#ARG1 = sigproc file
#ARG2 = telescope
#ARG3 = lower freq limit
#ARG4 = upper freq limit 

try:
    with open(sys.argv[1],'r') as infile:
        for line in infile:
            fields = line.split()
            mjd1 = fields[0]
            date = fields[1]
            timex = fields[2]
            source = fields[3]
            tel = fields[4]
            freq = float(fields[5])
            mjd2 = fields[6]
            if tel != sys.argv[2]:
                print mjd1[:5]
            if tel == sys.argv[2]:
                if freq > float(sys.argv[4]):
                    print mjd1[:5]
                if freq < float(sys.argv[3]):
                    print mjd1[:5]
except IndexError:
    print "Wrong number of args"
    print "\n <file> <telescope> <lower freq> <upper freq>"
    sys.exit(9)
