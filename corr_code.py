#!/usr/bin/python

# Cross correlates Pdot with an array of profile residuals and calculates the Spearman correlation

import argparse
import pylab as pb
import numpy as np
import math
import matplotlib
import matplotlib.pyplot as plt
import scipy as sc
import scipy.signal as scisig
import sys
import os 
from os.path import basename
from scipy.stats.stats import spearmanr
from matplotlib import gridspec
from matplotlib.ticker import NullFormatter

import matplotlib as mpl
label_size = 10
mpl.rcParams['xtick.labelsize'] = label_size
mpl.rcParams['ytick.labelsize'] = label_size

# Read command line arguments
#------------------------------
parser = argparse.ArgumentParser(description='Pulsar nudot variability studies using GPs')
parser.add_argument('-s','--series', help='File containing pdot series', required=True)
parser.add_argument('-a','--array', help='File containing profile residuals array to be correlated with pdot', required=True)
parser.add_argument('-l','--l1', nargs=1 ,default = (0), help='maximum lag required', type = int, required = False)
#------------------------------

# Give names to the arguements

args = parser.parse_args()
ser = args.series
arr = args.array
maxlag = args.l1[0]
series = np.loadtxt(ser)
array = np.loadtxt(arr)

# Provide average profile with standard deviation for the middle panel

median = np.loadtxt('2148+63_daily_med.txt')
sdev = np.loadtxt('2148+63_daily_std.txt')
bins_list = np.arange(array.shape[0])

# start and end bin for the average profile and standard dev in the middle panel

startbinzoom = 75
endbinzoom = 125
#dev_ticks=[0,25,50]
dev_ticks=[0,1,2]

sdev_zoom = sdev[startbinzoom:endbinzoom]
median_zoom = median[startbinzoom:endbinzoom]

#strong_profile = np.loadtxt("strong.txt", unpack=True)
#strong_zoom = strong_profile[startbinzoom:endbinzoom]

#weak_profile = np.loadtxt("weak.txt", unpack=True)
#weak_zoom = weak_profile[startbinzoom:endbinzoom]
low = []
upp = []

# Grey band indentifies 2 sigma

for i in range(len(median_zoom)):
    upp.append(median_zoom[i]+2*sdev_zoom[i])
    low.append(median_zoom[i]-2*sdev_zoom[i])

startbin = 0
endbin = array.shape[0]-1

# Find the shortest number of indicies out of the two correlated series

if array.shape[1] > series.shape[0]:
    shortest = series.shape[0]
else:
    shortest = array[0,:].shape[0]

off_std = []

for i in range(len(median_zoom)):
    if median_zoom[i] < np.max(median_zoom)/30.0:
        array[i,:] = 0

for i in range(len(median)):
    if median[i] < np.max(median_zoom)/30.0:
        off_std.append(sdev[i])
mean_off_std = np.mean(off_std)

for i in range(len(sdev_zoom)):
    sdev_zoom[i] = sdev_zoom[i]/mean_off_std
    
# Start date of series 1 and series 2:

start1 = 47905
start2 = 47905
absstart = abs(start1 - start2)

# Extend the nudot series in order to accommodate plus minus 500 days of lag

mean_nu_dot = np.mean(series[:shortest+absstart])
nu_dot_extended = np.zeros((maxlag*2+shortest))
nu_dot_extended[:] = mean_nu_dot
for i in range(shortest):
    nu_dot_extended[maxlag+i] = series[i+absstart-1]

# make an array of correlation coefficients

corr_map_spearman = np.zeros((1+maxlag*2,array.shape[0]))
p_value = np.zeros((1+maxlag*2,array.shape[0]))
jlist = []
ilist = [] 


plot_count = 1
for i in range(array.shape[0]):
    print "*** Corr plot",i,"of",array.shape[0],"produced ***"

    for j in range(1+maxlag*2):
        corr_map_spearman[j,i], p_value[j,i] = spearmanr(nu_dot_extended[j:j+shortest],array[i,:shortest])

# get rid of nan values

corr_map_spearman = np.nan_to_num(corr_map_spearman)
corr_map_p = np.nan_to_num(p_value)

# group the correlations

corr_grouping = 1
corr_resamp = np.zeros((corr_map_spearman.shape[0],corr_map_spearman.shape[1]/corr_grouping))
corr_resamp_p = np.zeros((corr_map_p.shape[0],corr_map_p.shape[1]/corr_grouping))

for j in range(corr_map_spearman.shape[0]):
    corr_resamp[j,:] = scisig.resample(corr_map_spearman[j,:],corr_map_spearman.shape[1]/corr_grouping)
    corr_resamp_p[j,:] = scisig.resample(corr_map_p[j,:],corr_map_p.shape[1]/corr_grouping)

# plot

fig2 = plt.figure()
fig2.text(0.05, 0.845, 'Std Dev. of\nPhase Bins', ha='center', va='center', rotation='vertical', size=10)
fig2.text(0.05, 0.63, 'Normalised\nFlux Density', ha='center', va='center', rotation='vertical', size=10)
fig2.text(0.05, 0.27, 'Phase Lag (Days)', ha='center', va='center', rotation='vertical',size=10)

fig2.text(0.157, 0.865, '(A)', ha='center', va='center', size=12)
fig2.text(0.157, 0.745, '(B)', ha='center', va='center', size=12)
fig2.text(0.157, 0.385, '(C)', ha='center', va='center', size=12)


outer = gridspec.GridSpec(2, 1, height_ratios=[1,6])
gs1 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec = outer[0])
axn = plt.subplot(gs1[0])
plt.plot(sdev_zoom,'k')
plt.xlim(startbin,endbin)
axn.xaxis.set_visible(False)
#plt.ylim(0,np.max(sdev_zoom*1.1))
plt.ylim([0, 2])
plt.yticks(dev_ticks)
xlim1 = startbin/corr_grouping
xlim2 = endbin/corr_grouping
xlocation = np.linspace(xlim1,xlim2,5)
xlabels=[]
for i in range(xlocation.shape[0]):
    xlabels.append(round(((xlocation[i]-xlim1)/(1023.0/corr_grouping)),2))
plt.xticks(xlocation,xlabels)

gs2 = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec = outer[1], hspace = .12)
ax3 = plt.subplot(gs2[0])
xlabels = []

for i in range(xlocation.shape[0]):
        xlabels.append(round(((xlocation[i]-xlim1)/(1023.0/corr_grouping)),2))
plt.xticks(xlocation,xlabels)

plt.plot(median_zoom,'k--')
#plt.plot(strong_zoom, 'r-')
#plt.plot(weak_zoom, 'b-')
print "MEDIAN IS",median
print len(bins_list),len(low),len(upp)
plt.fill_between(bins_list,low,upp,color='k',alpha = 0.1)
plt.xlim(startbin,endbin)
plt.ylim(np.min(low),1.1*np.max((np.amax(upp))))
ax3.xaxis.set_ticklabels([])
plt.grid(True)
ax3.xaxis.set_major_formatter(NullFormatter())
ax4 = plt.subplot(gs2[1])
ylocation = [0,maxlag/2,maxlag,3*maxlag/2,2*maxlag]
ylabels = [-maxlag, -maxlag/2,0,maxlag/2,maxlag]
plt.yticks(ylocation,ylabels)
max_corr = np.amax(corr_resamp)
min_corr = np.amin(corr_resamp)
absmax  = np.max((max_corr, np.abs(min_corr)))
plt.imshow(corr_resamp,aspect='auto',cmap = "RdBu_r", vmin = -absmax, vmax = absmax)
plt.xlim(xlim1,xlim2)
plt.subplots_adjust(hspace=0.0)
plt.xlabel('Profile Phase (Fraction of Pulse Period)',fontsize=10)
plt.xticks(xlocation,xlabels)
plt.grid()
cbaxes_corr = fig2.add_axes([0.45, 0.451, 0.45, 0.01])
cb_corr = plt.colorbar(cax = cbaxes_corr,orientation="horizontal")
cbaxes_corr.tick_params(labelsize=8)
cb_corr.update_ticks()
plt.savefig('srcc.png')
plt.clf()

print "The mean of all the correlation coefficients is:",np.mean(corr_resamp)
print "The standard deviation of all the correlation coefficients is:",np.std(corr_resamp)
print "MEAN NU DOT",mean_nu_dot
