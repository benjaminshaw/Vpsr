#!/usr/bin/python

import argparse
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42


parser = argparse.ArgumentParser(description='Spin-down/variability comparison plotter')
parser.add_argument('-f', '--filename', help='Inferred array file', required=True)
parser.add_argument('-p', '--pulsar', help='Pulsar name', required=True)
parser.add_argument('-b', '--boundaries', help='File containing the on pulse regions', required=True)
parser.add_argument('-s', '--nudot', help='Spin down rate', required=False)
args = parser.parse_args()


pulsar = args.pulsar
mjd = np.loadtxt('./{0}/mjd_norm.txt'.format(pulsar))
interval = np.loadtxt('./{0}/{0}_interval.dat'.format(pulsar))
ylimits = [0, 0]
try:
    ylimits = np.loadtxt('./{0}/{0}_ylimits.dat'.format(pulsar))
    print "Found ylimits file for nudot"
except IOError:
    pass
filename = args.filename
inferredarray = np.loadtxt(filename)
boundaries = args.boundaries
with open(boundaries, 'r') as b:
    lines = b.readlines() 
    leading = float(lines[0])
    trailing = float(lines[1])
    nbins = float(lines[2])
bins = trailing - leading
peakline = nbins/4-leading
#yaxis = []
#yaxis.append(np.linspace(0, bins/nbins, bins))
yaxis = np.linspace(0, bins/nbins, bins)
#print yaxis ; sys.exit(9)
maxdifference = np.amax(inferredarray)
mindifference = np.amin(inferredarray)
limitdifference = np.max((maxdifference, np.abs(mindifference)))
mjdinfer = np.arange(mjd[0],mjd[-1],interval)

#prepare plot
xbins = inferredarray.shape[1]
ybins = inferredarray.shape[0]
fig=plt.figure()


fig.set_size_inches(16,10)

#fig.set_size_inches(10.7,10) #2043 DFB
#plt.subplots_adjust(wspace=0, hspace=0.05, left=0.2) #2043 DFB



#fig.set_size_inches(6.85,10) #2035 DFB
#plt.subplots_adjust(wspace=0, hspace=0.05, left=0.2) #2035 DFB


#fig.set_size_inches(6.7,10) #1828 DFB
#plt.subplots_adjust(wspace=0, hspace=0.05, left=0.2) #1828 DFB

#fig.set_size_inches(6.3,10) #1642 DFB
#plt.subplots_adjust(wspace=0, hspace=0.05, left=0.2) #1642 DFB

#fig.set_size_inches(7.0,10) #1540 DFB
#plt.subplots_adjust(wspace=0, hspace=0.05, left=0.2) #1540 DFB

#fig.set_size_inches(6.85,10) #0740 DFB
#plt.subplots_adjust(wspace=0, hspace=0.05, left=0.2) #0740 DFB

ax=fig.add_subplot(2,1,1)
ax.xaxis.set_visible(False)
plt.imshow(inferredarray*1.0, aspect="auto", cmap="RdBu_r", vmin=-limitdifference, vmax=limitdifference)
for i in range(mjd.shape[0]):
    plt.vlines((mjd[i]-mjdinfer[0])/float(interval), 0, ybins, linestyle='dotted')
plt.hlines(peakline,0,xbins)
plt.ylabel("Fraction of a turn", fontsize=20)
plt.xlabel("MJD", fontsize=20)
xlocs = np.arange(xbins, step=1000/float(interval))
xticklabels = []
for i in xlocs:
    xticklabels.append(np.int(mjdinfer[i]))
ylocs = np.linspace(0, ybins, 10)
yticklabels = []
for i in ylocs[:-1]:
    yticklabels.append(round(yaxis[i],3))
plt.xticks(xlocs, xticklabels, rotation="horizontal")   
plt.yticks(ylocs, yticklabels)
plt.tick_params(axis='both', which='major', labelsize=15)
cbaxes3 = fig.add_axes([0.35, 0.94, 0.55, 0.01])
cb3 = plt.colorbar(cax=cbaxes3, orientation="horizontal") 
#cb3 = plt.colorbar(cax=cbaxes3, ticks=[-6.0, -3.0, 0.0, 3.0, 6.0], orientation="horizontal") #1642 DFB
#cb3 = plt.colorbar(cax=cbaxes3, ticks=[-16.0, -8.0, 0.0, 8.0, 16.0], orientation="horizontal") #1828 DFB
#cb3 = plt.colorbar(cax=cbaxes3, ticks=[-1.2, -0.6, 0.0, 0.6, 1.2], orientation="horizontal") #2035 DFB
cb3.update_ticks()
cb3.ax.tick_params(labelsize=13)
ax = fig.add_subplot(2,1,2)
mjd_sd, nudot_sd, err_sd  = np.loadtxt('{0}/mjd_nudot_error.dat'.format(pulsar), unpack=True)
if args.nudot:
    nudot_value = args.nudot
    nudot_value = float(nudot_value) 
first_nudot = str(nudot_sd[0])
order = first_nudot[-2:]
order = "1e-" + str(order)
order = float(order)
plt.plot(mjd_sd, nudot_sd/(order), 'k')
plt.(mjd_sd, nudot_sd/(order), 'k')
#plt.scatter(55000, -7.4, marker='o', color='lime')
range_nudots = []
for i in range(0, len(mjd_sd)):
    if mjd_sd[i] >= mjdinfer[0] and mjd_sd[i] <= mjdinfer[-1]:
        range_nudots.append(nudot_sd[i])
#plt.xlim(mjd_sd[2], mjd_sd[-1]) #Uncomment for 1828 AFB
plt.xlim(mjdinfer[0],mjdinfer[-1])
#plt.xlim(49000,mjdinfer[-1])
#plt.ylim([min(range_nudots)/order, max(range_nudots)/order])
if abs(ylimits[0]) > 0 :
    plt.ylim([ylimits[0], ylimits[1]])
else:
    plt.ylim([min(range_nudots)/order, max(range_nudots)/order])



#plt.ylim([-6.08, -6.02])

###############################################
#Uncomment for 0919 DFB

#plt.axvline(55152, ymin=0, ymax=1)

################################################
#Uncomment the following for 0740 AFB

#plt.ylim([-6.1, -6.00]) 
#plt.axvline(47625, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(48332, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(51770, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(52028, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(53083, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(53068, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(55020, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
################################################

################################################
#Uncomment the following the 0740 DFB

#plt.axvline(55020, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(56727, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
################################################

#Uncomment the following for 1822 AFB
#plt.axvline(49942, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(50314, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(51060, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(52056, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(52810, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(53734, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)
#plt.axvline(54115, ymin=0, ymax=1, linestyle='dashed', color='k', linewidth=2)



plt.grid()
plt.xlabel("Modified Julian Day", fontsize=20)
plt.ylabel(r'$\dot{\nu}$ (%s Hz/s)' % order, fontsize=20)
plt.tick_params(axis='both', which='major', labelsize=15)

#plt.grid(b=None, which='both', axis='x')
#plt.grid()
if args.nudot:
    plt.axhline(y=nudot_value, xmin=0, xmax=1, color='k', hold=None)

#plt.tight_layout()
#plt.savefig("maps.eps", format='eps', dpi=400)
plt.savefig("maps_scale.eps", format='eps', dpi=400)
#plt.savefig("maps.png", format='png', dpi=400)
#plt.savefig("maps.pdf", format='pdf', dpi=1000)
#plt.savefig("maps.jpg")
plt.show() 
