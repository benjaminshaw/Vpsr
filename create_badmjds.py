#!/usr/bin/python

import sys
from numpy import *

#ARG1 = sigproc file
#ARG2 = telescope we want
#ARG3 = lower freq limit
#ARG4 = upper freq limit 
#ARG5 = bw

try:
    with open(sys.argv[1],'r') as infile:
        for line in infile:
            fields = line.split()
            obs = fields[0]
            mjd = fields[1]
            freq = fields[2]
            bw = fields[3]
            tel = fields[4]
            if tel != sys.argv[2]:# or abs(float(bw)) != float(sys.argv[5]):
                print mjd
            if tel == sys.argv[2]: # and abs(float(bw)) == float(sys.argv[5]):
                if float(freq) > float(sys.argv[4]):
                    print mjd
                if float(freq) < float(sys.argv[3]):
                    print mjd
except IndexError:
    print "Wrong number of args"
    print "\n <file> <telescope> <lower freq> <upper freq> <bw>"
    sys.exit(9)
