#!/usr/bin/python

from numpy import *
import matplotlib.pyplot as plt
import sys

powerfile = sys.argv[1]
sigmafile = sys.argv[2]
lower_b = float(sys.argv[3])
upper_b = float(sys.argv[4])
name = sys.argv[5]

power = loadtxt(powerfile, unpack=True)
sigma = loadtxt(sigmafile, unpack=True)
phase = []
for i in range(0, len(power)):
    phase.append((float(i) / 400.) - (lower_b))

#print phase ; sys.exit(9)

phase2 = []
for i in range(0, len(sigma)):
    phase2.append((float(i) / 400.0) - (lower_b))

ulim = []
llim = []
for i in range(0, len(sigma)):
    ulim.append(power[i] + 2*sigma[i])
    llim.append(power[i] - 2*sigma[i])

label_size = 20
plt.rcParams['xtick.labelsize'] = label_size 
plt.plot(phase, power, 'k--')
#plt.plot(phase2, ulim) 
#plt.plot(phase2, llim) 
plt.fill_between(phase2, llim, ulim, color = 'k', alpha = 0.2)

plt.yticks([])
#plt.xlim([0, upper_b - lower_b])
plt.text(0.0005, 1.37, name, size=20)
#plt.ylabel("Normalised power")
#plt.xlabel("Pulse phase (turns)", size=20)
plt.tight_layout()

#plt.savefig("template_for_paper.png")
plt.show()
